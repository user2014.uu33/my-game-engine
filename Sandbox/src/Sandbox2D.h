#pragma once
#include "SiEngine.h"
#include "sepch.h"

class Sandbox2D :public SiEngine::Layer
{
public:
	Sandbox2D();
	virtual ~Sandbox2D() = default;

	virtual void OnAttach() override;
	virtual void OnDetach() override;

	void OnUpdate(SiEngine::Timestep ts) override;
	virtual void OnImGuiRender() override;
	void OnEvent(SiEngine::Event& event) override;
private:
	SiEngine::OrthographicCameraController m_CameraController;

	//temp
	SiEngine::Ref<SiEngine::VertexArray> m_SquareVA;
	SiEngine::Ref<SiEngine::Shader> m_FlatColorShader;

	SiEngine::Ref<SiEngine::Texture2D> m_Board;

	glm::vec4 m_SquareColor = { 0.2f, 0.3f, 0.8f, 1.0f };
};