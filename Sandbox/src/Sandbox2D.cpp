#include "Sandbox2D.h"
#include "imgui/imgui.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <chrono>

Sandbox2D::Sandbox2D() : Layer("Sandbox2D"), m_CameraController(1280.0f / 720.0f)
{

}

void Sandbox2D::OnAttach() 
{
	SE_PROFILE_FUNCTION();
	m_Board = SiEngine::Texture2D::Create("assets/textures/image.png");
}
void Sandbox2D::OnDetach() 
{
	SE_PROFILE_FUNCTION();
}

void Sandbox2D::OnUpdate(SiEngine::Timestep ts)
{
	SE_PROFILE_FUNCTION();

	//Update
	m_CameraController.OnUpdate(ts);

	//Render
	{
		SE_PROFILE_SCOPE("Renderer Prep ");
		SiEngine::RenderCommand::SetClearColor({ 0.1f, 0.1f, 0.1f, 1 });
		SiEngine::RenderCommand::Clear();

	}

	{
		SE_PROFILE_SCOPE("Renderer Draw ");
		SiEngine::Renderer2D::BeginScene(m_CameraController.GetCamera());
		//SiEngine::Renderer2D::DrawRotatedQuad({ -1.0f, 0.0f }, { 0.8f, 0.8f }, glm::radians(-45.0f), { 0.8f, 0.2f, 0.3f, 1.0f });
		SiEngine::Renderer2D::DrawQuad({ -1.0f, 0.0f }, { 0.8f, 0.8f }, { 0.8f, 0.2f, 0.3f, 1.0f });
		SiEngine::Renderer2D::DrawQuad({ 0.5f,-0.5f }, { 0.5f, 0.75f }, { 0.2f, 0.3f, 0.8f, 1.0f });
		SiEngine::Renderer2D::DrawQuad({ -5.0f, -5.0f, -0.1f }, { 10.0f, 10.0f }, m_Board, 10.0f);
		SiEngine::Renderer2D::DrawQuad({ -0.5f, -0.5f, 0.0f }, { 1.0f, 1.0f }, m_Board, 20.0f);
		
		SiEngine::Renderer2D::EndScene();
	}
}
void Sandbox2D::OnImGuiRender()
{
	SE_PROFILE_FUNCTION();

	ImGui::Begin("Settings");
	ImGui::ColorEdit4("Square Color", glm::value_ptr(m_SquareColor));

	ImGui::End();
}
void Sandbox2D::OnEvent(SiEngine::Event& event)
{
	m_CameraController.OnEvent(event);
}