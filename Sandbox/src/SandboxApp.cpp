#include <SiEngine.h>

#include <SiEngine/EntryPoint.h>

#include "Platform/OpenGL/OpenGLShader.h"
#include "imgui/imgui.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Sandbox2D.h"
#include "SiEngine/ImGui/ImGuiLayer.h"
#include "SiEngine/Renderer/RendererAPI.h"

class ExampleLayer : public SiEngine::Layer
{
public:
	ExampleLayer() : Layer("Example"), m_CameraController(1280.0f / 720.0f, true)
	{
		m_VertexArray = SiEngine::VertexArray::Create();

		float vertices[3 * 7] = {
			-0.8f, -0.5f, 0.0f, 0.8f, 0.2f, 0.8f, 1.0f,
			 0.5f, -0.5f, 0.0f, 0.2f, 0.3f, 0.8f, 1.0f,
			 0.0f, 0.5f, 0.0f, 0.8f, 0.8f, 0.2f, 1.0f,
		};

		SiEngine::Ref<SiEngine::VertexBuffer> VertexBuffer;
		VertexBuffer = SiEngine::VertexBuffer::Create(vertices, sizeof(vertices));
		SiEngine::BufferLayout layout =
		{
			{ SiEngine::ShaderDataType::Float3, "a_Position" },
			{ SiEngine::ShaderDataType::Float4, "a_Color" }
		};

		VertexBuffer->SetLayout(layout);
		m_VertexArray->AddVertexBuffer(VertexBuffer);

		uint32_t indices[3] = { 0, 1, 2 };
		SiEngine::Ref<SiEngine::IndexBuffer> indexBuffer;
		indexBuffer = SiEngine::IndexBuffer::Create(indices, sizeof(indices) / sizeof(uint32_t));
		m_VertexArray->SetIndexBuffer(indexBuffer);

		m_SquareVA = SiEngine::VertexArray::Create();

		float squareVertices[5 * 4] = {
			-0.5f, -0.5f, 0.0f, 0.0f, 0.0f,
			 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
			 0.5f,  0.5f, 0.0f, 1.0f, 1.0f,
			-0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
		};

		SiEngine::Ref<SiEngine::VertexBuffer> squareVB;
		squareVB = SiEngine::VertexBuffer::Create(squareVertices, sizeof(squareVertices));

		squareVB->SetLayout({
			{ SiEngine::ShaderDataType::Float3, "a_Position" }, 
			{ SiEngine::ShaderDataType::Float2, "a_TexCoord" }
			});

		m_SquareVA->AddVertexBuffer(squareVB);

		uint32_t squareIndices[6] = { 0, 1, 2, 2, 3, 0 };

		SiEngine::Ref<SiEngine::IndexBuffer> squareIB;
		squareIB = SiEngine::IndexBuffer::Create(squareIndices, sizeof(squareIndices) / sizeof(uint32_t));
		m_SquareVA->SetIndexBuffer(squareIB);

		std::string vertexSrc = R"(
			#version 330 core
		
			layout(location = 0) in vec3 a_Position;
			layout(location = 1) in vec4 a_Color;

			uniform mat4 u_ViewProjection;
			uniform mat4 u_Transform;
			
			out vec3 v_Position;	
			out vec4 v_Color;
			void main()
			{
				v_Position = a_Position;
				v_Color = a_Color;
				gl_Position = u_ViewProjection * u_Transform * vec4(a_Position, 1.0);
			}
		)";

		std::string fragmentSrc = R"(
			#version 330 core
		
			layout(location = 0) out vec4 color;

			in vec3 v_Position;
			in vec4 v_Color;
				
			void main()
			{
				color = vec4(v_Position * 0.5 + 0.5, 1.0);
				color = v_Color;
			}
		)";

		m_Shader = SiEngine::Shader::Create("VertexPosColor", vertexSrc, fragmentSrc);

		std::string flatColorShaderVertexSrc = R"(
			#version 330 core
		
			layout(location = 0) in vec3 a_Position;

			uniform mat4 u_ViewProjection;
			uniform mat4 u_Transform;
			
			out vec3 v_Position;	

			void main()
			{
				v_Position = a_Position;
				gl_Position = u_ViewProjection * u_Transform * vec4(a_Position, 1.0);
			}
		)";

		std::string flarColorShaderFragmentSrc = R"(
			#version 330 core
		
			layout(location = 0) out vec4 color;

			in vec3 v_Position;

			uniform vec3 u_Color;
				
			void main()
			{
				color = vec4(u_Color, 1.0);
			}
		)";

		m_FlatColorShader = SiEngine::Shader::Create("FlatColor", flatColorShaderVertexSrc, flarColorShaderFragmentSrc);

		auto textureShader = m_ShaderLibrary.Load("assets/shaders/Texture.glsl");

		m_Texture = SiEngine::Texture2D::Create("assets/textures/image.png");
		m_Logo = SiEngine::Texture2D::Create("assets/textures/logo.png");

		std::dynamic_pointer_cast<SiEngine::OpenGLShader>(textureShader)->Bind();
		std::dynamic_pointer_cast<SiEngine::OpenGLShader>(textureShader)->UploadUniformInt("u_Texture", 0);
	}

	void OnUpdate(SiEngine::Timestep ts) override
	{
		SE_TRACE("Delta time: {0}s ({1}ms)", ts.GetSeconds(), ts.GetMilliSeconds());

		//Update
		m_CameraController.OnUpdate(ts);

		//Render
		SiEngine::RenderCommand::SetClearColor({ 0.1f, 0.1f, 0.1f, 1 });
		SiEngine::RenderCommand::Clear();

		SiEngine::Renderer::BeginScene(m_CameraController.GetCamera());

		glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(0.1f));

		std::dynamic_pointer_cast<SiEngine::OpenGLShader>(m_FlatColorShader)->Bind();
		std::dynamic_pointer_cast<SiEngine::OpenGLShader>(m_FlatColorShader)->UploadUniformFloat3("u_Color", m_SquareColor);
		 
		for (int i = 0; i < 20; ++i)
			for (int j = 0; j < 20; ++j)
			{
				glm::vec3 pos(j * 0.11f, i * 0.11f, 0.0f);
				glm::mat4 transform = glm::translate(glm::mat4(1.0f), pos) * scale;
				SiEngine::Renderer::Submit(m_FlatColorShader, m_SquareVA, transform);
			}

		auto textureShader = m_ShaderLibrary.Get("Texture");

	    m_Texture->Bind();
		SiEngine::Renderer::Submit(textureShader, m_SquareVA, glm::scale(glm::mat4(1.0f), glm::vec3(1.5f)));

		m_Logo->Bind();
		SiEngine::Renderer::Submit(textureShader, m_SquareVA, glm::scale(glm::mat4(1.0f), glm::vec3(1.5f)));

		/*Triangle
		SiEngine::Renderer::Submit(m_Shader, m_VertexArray);*/

		SiEngine::Renderer::EndScene();
	}

	void OnImGuiRender()
	{
		ImGui::Begin("Settings");
		ImGui::ColorEdit3("Square Color", glm::value_ptr(m_SquareColor));
		ImGui::End();
	}

	void OnEvent(SiEngine::Event& event) override
	{
		m_CameraController.OnEvent(event);

		if (event.GetEventType() == SiEngine::EventType::WindowResize)
		{
			auto& re = (SiEngine::WindowResizeEvent&)event;
			/*float zoom = (float)re.GetWidth() / 1280.0f;
			m_CameraController.SetZoomLevel(zoom);*/
		}
	}

private:
	SiEngine::ShaderLibrary m_ShaderLibrary;
	SiEngine::Ref<SiEngine::Shader> m_Shader;
	SiEngine::Ref<SiEngine::VertexArray> m_VertexArray;
	 
	SiEngine::Ref<SiEngine::Shader> m_FlatColorShader;
	SiEngine::Ref<SiEngine::VertexArray> m_SquareVA;

	SiEngine::Ref<SiEngine::Texture2D> m_Texture, m_Logo;

	SiEngine::OrthographicCameraController m_CameraController;

	glm::vec3 m_SquareColor = { 0.2f, 0.3f, 0.8f };
};

class Sandbox : public SiEngine::Application 
{
public:
	Sandbox() 
	{
		//PushLayer(new ExampleLayer());
		PushLayer(new Sandbox2D());
	}
	~Sandbox()
	{

	}
};

SiEngine::Application* SiEngine::CreateApplication() {
	return new Sandbox();
}