#pragma once

#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

namespace SiEngine {
	class SIENGINE_API Log
	{
	public:
		static void Init();
		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
	};
}
//Core log macros
#define SE_CORE_TRACE(...)  ::SiEngine::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define SE_CORE_INFO(...)   ::SiEngine::Log::GetCoreLogger()->info(__VA_ARGS__)
#define SE_CORE_WARN(...)   ::SiEngine::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define SE_CORE_ERROR(...)  ::SiEngine::Log::GetCoreLogger()->error(__VA_ARGS__)
#define SE_CORE_FATAL(...)  ::SiEngine::Log::GetCoreLogger()->fatal(__VA_ARGS__)
//Clint log macros
#define SE_TRACE(...)       ::SiEngine::Log::GetClientLogger()->trace(__VA_ARGS__)
#define SE_INFO(...)		::SiEngine::Log::GetClientLogger()->info(__VA_ARGS__)
#define SE_WARN(...)		::SiEngine::Log::GetClientLogger()->warn(__VA_ARGS__)
#define SE_ERROR(...)		::SiEngine::Log::GetClientLogger()->error(__VA_ARGS__)
#define SE_FATAL(...)		::SiEngine::Log::GetClientLogger()->fatal(__VA_ARGS__)
//if dist build
//#define SE_CORE_INFO