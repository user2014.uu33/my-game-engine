#pragma once
#ifdef SE_PLATFORM_WINDOWS
#include "Debug/Instrumentor.h"
extern SiEngine::Application* SiEngine::CreateApplication();
 
int main(int args, char** argv) {
	SiEngine::Log::Init();

	SE_PROFILE_BEGIN_SESSION("Startup", "SiEnginePrfoile-Startup.json");
	auto app = SiEngine::CreateApplication();
	SE_PROFILE_END_SESSION();

	SE_PROFILE_BEGIN_SESSION("Runtime", "SiEnginePrfoile-Runtime.json");
	app->Run();
	SE_PROFILE_END_SESSION();

	SE_PROFILE_BEGIN_SESSION("Startup", "SiEnginePrfoile-Shutdown.json");
	delete app;
}
#endif