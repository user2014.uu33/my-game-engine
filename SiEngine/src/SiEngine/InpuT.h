#pragma once
#include "SiEngine/Core.h"
#include "SiEngine/MouseCodes.h"
#include "SiEngine/Events/KeyCodes.h"


namespace SiEngine
{
	class SIENGINE_API InpuT
	{
	public:
		inline static bool IsKeyPressed(int keycode) { return s_Instanse->IsKeyPressedImpl(keycode); }

		inline static bool IsMouseButtonPressed(int button) { return s_Instanse->IsMouseButtonPressedImpl(button); }
		inline static std::pair<float, float> GetMousePosition() { return s_Instanse->GetMousePositionImpl(); }
		inline static float GetMouseX() { return s_Instanse->GetMouseXImpl(); }
		inline static float GetMouseY() { return s_Instanse->GetMouseYImpl(); }
	protected:
		virtual bool IsKeyPressedImpl(int keycode) = 0;

		virtual bool IsMouseButtonPressedImpl(int button) = 0;
		virtual std::pair<float, float> GetMousePositionImpl() = 0;
		virtual float GetMouseXImpl() = 0;
		virtual float GetMouseYImpl() = 0;
	private:
		static InpuT* s_Instanse;
	};
}
