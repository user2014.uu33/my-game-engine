#pragma once
#include "SiEngine/Layer.h"
#include "SiEngine/Events/KeyEvent.h"
#include "SiEngine/Events/ApplicationEvent.h"
#include "SiEngine/Events/MouseEvent.h"

namespace SiEngine
{
	class SIENGINE_API ImGuiLayer : public Layer
	{
	public:
		ImGuiLayer();
		~ImGuiLayer() = default;

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		//virtual void OnImGuiRender() override;
		
		void Begin();
		void End();

		void BlockEvents(bool block) { m_BlockEvents = block; }
		void SetDarkThemeColors();
	private:
		bool m_BlockEvents = true;
		float m_Time = 0.0f;
	};
}