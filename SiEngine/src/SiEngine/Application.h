#pragma once
#include "Core.h"
#include "Window.h"
#include "SiEngine/LayerStack.h"
#include "Events/Event.h"
#include "Events/ApplicationEvent.h"
#include "SiEngine/ImGui/ImGuiLayer.h"

#include "SiEngine/Core/Timestep.h"

#include "SiEngine/Renderer/Shader.h"
#include "SiEngine/Renderer/Buffer.h"
#include "SiEngine/Renderer/VertexArray.h"

#include "SiEngine/Renderer/OrthographicCamera.h"

namespace SiEngine {

	class SIENGINE_API Application {
		public:
			Application();
			virtual ~Application();
			void Run();
			void OnEvent(Event& event);
		
			void PushLayer(Layer* layer);
			void PushOverlay(Layer* layer);

			inline Window& GetWindow() { return *m_Window; }
			inline static Application& Get() { return *s_Instance; }

		private:
			bool OnWindowClose(WindowCloseEvent& event);
			bool OnWindowResize(WindowResizeEvent& event);
		private:
			std::unique_ptr<Window> m_Window;
			ImGuiLayer* m_ImGuiLayer;
			bool m_Running = true, m_Minimized = false;
			LayerStack m_LayerStack;
			float m_LastFrameStep = 0.0f;
		private:
			static Application* s_Instance;
	};
		//To be defined in CLIENT
		Application* CreateApplication();

}