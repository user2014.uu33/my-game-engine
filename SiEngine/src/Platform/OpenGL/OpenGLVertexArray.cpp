#include "sepch.h"
#include "OpenGLVertexArray.h"
#include "glad/glad.h"

namespace SiEngine
{
	static GLenum ShaderDataTypeToOpenGLBaseType(ShaderDataType type)
	{
		switch (type)
		{
			case SiEngine::ShaderDataType::Float:	return GL_FLOAT;
			case SiEngine::ShaderDataType::Float2:	return GL_FLOAT;
			case SiEngine::ShaderDataType::Float3:	return GL_FLOAT;
			case SiEngine::ShaderDataType::Float4:	return GL_FLOAT;
			case SiEngine::ShaderDataType::Mat3:	return GL_FLOAT;
			case SiEngine::ShaderDataType::Mat4:	return GL_FLOAT;
			case SiEngine::ShaderDataType::Int:		return GL_INT;
			case SiEngine::ShaderDataType::Int2:	return GL_INT;
			case SiEngine::ShaderDataType::Int3:	return GL_INT;
			case SiEngine::ShaderDataType::Int4:	return GL_INT;
			case SiEngine::ShaderDataType::Bool:	return GL_BOOL;
		}
		SE_CORE_ASSERT(false, "Unknown ShaderDataType");
		return 0;
	}

	OpenGLVertexArray::OpenGLVertexArray()
	{
		SE_PROFILE_FUNCTION();
		glCreateVertexArrays(1, &m_RendererID);
	}

	OpenGLVertexArray::~OpenGLVertexArray()
	{
		SE_PROFILE_FUNCTION();
		glDeleteVertexArrays(1, &m_RendererID);
	}

	void OpenGLVertexArray::Bind() const
	{
		SE_PROFILE_FUNCTION();
		glBindVertexArray(m_RendererID);
	}

	void OpenGLVertexArray::Unbind() const
	{
		SE_PROFILE_FUNCTION();
		glBindVertexArray(0);
	}

	void OpenGLVertexArray::AddVertexBuffer(const Ref<VertexBuffer>& vertexBuffer)
	{
		SE_PROFILE_FUNCTION();
		SE_CORE_ASSERT(vertexBuffer->GetLayout().GetElements().size(), "Vertex Buffer has no layout!");

		glBindVertexArray(m_RendererID);
		vertexBuffer->Bind();

		uint32_t index = 0;

		const auto& layout = vertexBuffer->GetLayout();
		for (const auto& element : layout)
		{
			glEnableVertexAttribArray(index);
			glVertexAttribPointer(index, element.GetElementCount(),
				ShaderDataTypeToOpenGLBaseType(element.Type),
				element.Normalized ? GL_FALSE : GL_FALSE,
				layout.GetStride(),
				(const void*)element.Offset);
			++index;
		}
		m_VertexBuffer.push_back(vertexBuffer);
	}

	void OpenGLVertexArray::SetIndexBuffer(const Ref<IndexBuffer>& indexBuffer)
	{
		SE_PROFILE_FUNCTION();
		glBindVertexArray(m_RendererID);
		indexBuffer->Bind();

		m_IndexBuffer = indexBuffer;
	}

}