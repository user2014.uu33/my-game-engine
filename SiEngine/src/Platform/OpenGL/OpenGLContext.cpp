#include "sepch.h"
#include "OpenGLContext.h"

#include "SiEngine/Log.h"

#include <GLFW/glfw3.h>
#include "glad/glad.h"

namespace SiEngine
{
	OpenGLContext::OpenGLContext(GLFWwindow * windowHandle): m_WindowHandle(windowHandle)
	{
		SE_CORE_ASSERT(windowHandle, "Window handle is null!");
	}
	void OpenGLContext::Init()
	{
		SE_PROFILE_FUNCTION();
		glfwMakeContextCurrent(m_WindowHandle);
		int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
		SE_CORE_ASSERT(status, "Failed to initialize Glad!");

		SE_CORE_INFO("OpenGL Info: ");

		SE_CORE_INFO("  Vendor: {0}", glGetString(GL_VENDOR));
		SE_CORE_INFO("  Renderer: {0}", glGetString(GL_RENDERER));
		SE_CORE_INFO("  Version: {0}", glGetString(GL_VERSION));
	}
	void OpenGLContext::SwapBuffers()
	{
		SE_PROFILE_FUNCTION();
		glfwSwapBuffers(m_WindowHandle);
	}
}