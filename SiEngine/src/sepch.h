#pragma once

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <sstream>
#include <string>
#include <array>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "SiEngine/Log.h"
#include "Debug/Instrumentor.h"


#ifdef SE_PLATFORM_WINDOWS
	#include <Windows.h>
#endif