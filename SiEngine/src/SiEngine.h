#pragma once
#include "iostream"
#include "SiEngine/Application.h"
#include "SiEngine/Layer.h"
#include "SiEngine/Log.h"

#include "SiEngine/Core/Timestep.h"

#include "SiEngine/InpuT.h"
#include "SiEngine/MouseCodes.h"
#include "SiEngine/Events/KeyCodes.h"
#include "SiEngine/OrthographicCameraController.h"

#include "SiEngine/ImGui/ImGuiLayer.h"

// -- Renderer -----------------
#include "SiEngine/Renderer/Renderer.h"
#include "SiEngine/Renderer/Renderer2D.h"
#include "SiEngine/Renderer/RenderCommand.h"
#include "SiEngine/Renderer/Buffer.h"
#include "SiEngine/Renderer/Shader.h"
#include "SiEngine/Renderer/Texture.h"
#include "SiEngine/Renderer/VertexArray.h"
#include "SiEngine/Renderer/OrthographicCamera.h"
// ------------------------------

// --Entry Point-----------------
//#include "SiEngine/EntryPoint.h"
// ------------------------------